[previous](set03.md) : **Worksheet Set 4** : [next](set05.md)


## Key terms and concepts to understand
> &bull; Javascript  &bull; imperative programming  &bull; variable  &bull; value  &bull; literal  &bull; IPO operator &bull; expression  &bull; keyword  &bull; identifier  &bull;  data type &bull; strict mode  &bull; console  &bull;  algorithm  &bull; object-oriented programming  &bull; computer application  &bull;  text editor  &bull; client/server  &bull;  type conversion  &bull; function &bull method &bull;
> 
---

# Course material

### Read and analyze (analytical reading)
Observe and describe. Make notes. Explore the links provided in the material. Document what you have learned. When you come to an example, make your own version and explore it deeply. “Curiosity Is the engine of achievement.” —Sir Ken Robinson

1. [Javascript Objects, Functions, Event handling](https://ebajcar.github.io/web10199/material/material_js.html)

### Watch Suggested videos

- ...

### Class examples

- [Functions](syst10199/set4/functions_mix.html)

### Formulate answers to the following questions
1. What is an algorithm?


---


# Exercises
Set up a project for each problem. Keep all versions of your solution for each project.  For each problem, write out the
algorithm that you plan to use (and modify if your implementation deviates from your original
plan).


1. **Fibonacci numbers** 
    - (v.6) &mdash; Write a fully functional, unobtrusive Javascript solution.
    - (v.7) &mdash; Convert to object-oriented, unobtrusive Javascript solution.
2. **Temperature conversion** 
    - (v.4) &mdash; Write a fully functional, unobtrusive Javascript solution.   
    - (v.5) &mdash; Convert to object-oriented, unobtrusive Javascript solution.  
3. **Area of a circle** 
    - (v.3) &mdash; Write a fully functional, unobtrusive Javascript solution.   
    - (v.4) &mdash; Convert to object-oriented, unobtrusive Javascript solution.
4. **Quadratic roots** 
    - (v.4) &mdash; Write a fully functional, unobtrusive Javascript solution.
    - (v.5) &mdash; Convert to object-oriented, unobtrusive Javascript solution.
5. **Prime numbers** 
    - (v.4) &mdash; Write a fully functional, unobtrusive Javascript solution.
    - (v.5) &mdash; Convert to object-oriented, unobtrusive Javascript solution.
6. **Is it a palindrome** 
    - (v.3) &mdash; Write a fully functional, unobtrusive Javascript solution.
    - (v.4) &mdash; Convert to object-oriented, unobtrusive Javascript solution.
7. **Game of Casino Craps**
    - (v.4) &mdash; Write a fully functional, unobtrusive Javascript solution.
    - (v.5) &mdash; Convert to object-oriented, unobtrusive Javascript solution.
8. **Game of Tic Tac Toe**
    - (v.4) &mdash; Write a fully functional, unobtrusive Javascript solution.
    - (v.5) &mdash; Convert to object-oriented, unobtrusive Javascript solution.
9. **Game of Rock Paper Scissors**
    - (v.4) &mdash; Write a fully functional, unobtrusive Javascript solution.
    - (v.5) &mdash; Convert to object-oriented, unobtrusive Javascript solution.


  
---
> *The materials provided in class and in SLATE are protected by copyright. They are intended for the personal, educational uses of students in this course and should not be shared externally or on websites such as Course Hero or OneClass. Unauthorized distribution may result in copyright infringement and violation of Sheridan policies.*
> 
> **SYST10199 Web Programming @ Sheridan College**
